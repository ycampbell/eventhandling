import React from "react"
import "./TimerDisplasy.css"

class StartOver extends React.Component{
    constructor(props){
        super(props)
        this.timeRef= React.createRef();
        this.state= {
            time:[],
            count: 0
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleClick=this.handleClick.bind(this);
    }

    UNSAFE_componentDidMount(){
        console.log("The component did mount")
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        console.log("The component will receive", nextProps);
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log("SHOULD COMPONENT UPDATE")
        console.log("THIS IS THE LENGTH OF THE NEXT STata", nextState.time.length)
        if(nextState.time.length>0){
            return true;
        }
        else{
            clearInterval(this.timer)
            return false;
        }
           
    
    }

    UNSAFE_componentWillUpdate(nextProps, nextState){
        console.log("The component will update" ,nextProps, nextState)
    }

   componentDidUpdate(prevProps, prevState){
       // console.log("The component did update" ,prevProps, prevState)
        console.log("Updated array state",this.state.time)
                
    }

    componentWillUnmount(){
        clearInterval(this.timer)
    }

    

    handleChange(event){
        let time= event.target.value;
        this.setState({count:time}, ()=>{
            console.log(this.state.count)
        }) 
    }    


     handleClick(event){
      //  console.log("clicked");
        event.preventDefault();
        event.target.reset();
        const t=this.state.count;
        let i;
        this.setState({time: [...this.state.time, t]}, ()=>{
                 this.timer= setInterval(function(){
                     let times=this.state.time
                    for (i=0; i<times.length; i++){
                        if(times[i]<=0){
                            times.splice(i,1);
                            //splice the array and pass the
                        }else{
                            times[i]=times[i]-1;
                        }
                        
                    } 
                    this.setState({time:times}, ()=>{
                        console.log(this.state.time)
                    })
        }.bind(this),1000)

        })
    }

    render(){

        return (
            <form onSubmit={this.handleClick} className="flex-form">
            <div className="ya">
              <input name="time" type="number" ref={this.timeRef} className="timerInput boil" placeholder="Enter Boiling time" onChange={this.handleChange}></input>
              <input className="timerInput btn-submit" type="submit" value="Start Boiling"></input>
                
                <div className="counter">
                   {this.state.time.map(timer=>(
                       <div key={Math.random()} >
                           {timer===0? null : <div className="circle">{timer}</div>}
                       </div>
                   ))}
                </div>
                
              {/* <button className="btn-newitem" onClick={this.handleClick}>Start Boiling</button> */}
              </div>
            </form>
           
          );
    }
}

export default StartOver